const Express = require("express");
const Mongoose = require("mongoose");
const Bcrypt = require("bcrypt");
const Nanoid = require("nanoid");
const CookieParser = require("cookie-parser");
const User = require("./models/User");
const Post = require("./models/Post");
const Session = require("./models/Session");
const path = require('path');

const portfolioServer = Express();
const clientAppDirectory = path.join(__dirname, '../public', 'build');

Mongoose.connect(process.env.DATABASE_URL || "mongodb://localhost/portfolio", {useNewUrlParser: true, useUnifiedTopology: true}).then(r => console.log(" "));

Mongoose.connection.once("open", () => console.log("connected to database successfully."));

portfolioServer.use(Express.json());
portfolioServer.use(CookieParser());
portfolioServer.use(Express.static(clientAppDirectory));

authenticate = async (request, response, next) =>
{
    const {sessionId} = request.cookies;
    console.log("a request to authenticate a user with session ID " + sessionId + " came in");
    const userSession = await Session.findOne({sessionId: {$eq: sessionId}});
    const user = await User.findOne(
        {_id: {$eq: userSession.userId}},
        ["emailAddress", "username", "creationDate"]
    );
    if (user) {
        console.log("user authenticated");
        response.locals.user = user;
        return next();
    } else {
        console.log("user not authenticated");
        return response.sendStatus(401);
    }
};

clearExcessSessions = async (user) =>
{
    let sessions = await Session.find({userId: {$eq: user._id}}).sort({date: 1});
    console.log("User has "+ sessions.length + " sessions");
    while (sessions.length > 5) {
        await Session.deleteOne({sessionId: {$eq: sessions[0].sessionId}});
        sessions = await Session.find({userId: {$eq: user._id}}).sort({date: 1});
    }
};

portfolioServer.post("/api/users", async (request, response) => {
    const {emailAddress, username, password} = request.body;
    console.log("A request to create a user came in with the username: " + username + " and email: " + emailAddress);


    const emailExists = await User.findOne({emailAddress: {$eq: emailAddress}});
    if (emailExists){
        console.log("A user with the email address: " + emailAddress + " already exists. The request will be rejected.");
        return response.sendStatus(400);
    }

    const usernameExists = await User.findOne({username: {$eq: username}});
    if (usernameExists){
        console.log("A user with the username: " + username + " already exists. The request will be rejected.");
        return response.sendStatus(400);
    }

    Bcrypt.hash(password, 12, async (error, hash) => {
        if (error){
            console.log("An error occurred while hashing the password.");
            return response.sendStatus(400);
        }

        const newUser = await User.create({
            emailAddress: emailAddress,
            username: username,
            password: hash
        });

        console.log("The user: " + newUser.emailAddress + " has been created.");
        return response.sendStatus(200);
    });
});

portfolioServer.get("/api/user/", async (request, response) => {
    const {sessionId} = request.cookies;
    if (!sessionId){
        return;
    }
    const userSession = await Session.findOne({ sessionId: {$eq: sessionId } });
    if (userSession) {
        const user = await User.findOne(
            {_id: {$eq: userSession.userId}},
            {"emailAddress": 1, "username": 1, "creationDate": 1, "_id": 0}
        );
        return response.send(user).status(200);
    } else {
       return response.clearCookie("Session");
    }
});

portfolioServer.post("/api/sessions", async (request, response) => {
    console.log("An end user is attempting to login.");
    const {emailAddress, password} = request.body;
    const user = await User.findOne({ emailAddress: { $eq: emailAddress } });

    if (!user){
        console.log("No user was found with the email address: " + emailAddress);
        return response.sendStatus(400);
    }

    Bcrypt.compare(password, user.password, (error, result) => {

        if (error){
            console.log("An error occurred while checking the user's password hash.");
            return response.sendStatus(400);
        }

        if (result === true){
            console.log("User: " + user.emailAddress + " has logged in.");
            const sessionId = Nanoid(24);
            Session.create({
                sessionId: sessionId,
                userId: user._id
            });

            clearExcessSessions(user);

            response.cookie("sessionId", sessionId, {httpOnly: true});
            return response.sendStatus(200);
        }

        return response.sendStatus(400);
    });
});

portfolioServer.delete("/api/logout/", async (request, response) => {
    const sessionCookie = request.headers.cookie.split("sessionId=");
    const sessionId = sessionCookie[1];
    await Session.deleteOne({sessionId: {$eq: sessionId}});
    response.clearCookie("Session");
    console.log("The user has logged out.");
    return response.sendStatus(200);
});

portfolioServer.get("/api/posts", authenticate, async (request, response) => {
    console.log("A request came in to get all posts");
    const posts = await Post.find({});
    return response.send(posts).status(200);
});


portfolioServer.get("/api/posts/:username", authenticate, async (request, response) => {
        console.log("A request came in to get all posts by the user");
        const posts = await Post.find({ username: response.locals.user.username});
        return response.send(posts).status(200);
});

portfolioServer.post("/api/posts", authenticate, async (request, response) => {
        console.log("A request to create a new post came in.");
        const{message} = request.body;
        const {username} = response.locals.user;
        await Post.create({ username: username, message: message});
        return response.sendStatus(200);
});

portfolioServer.put("/api/posts/like/:postId", authenticate, async (request, response) => {
        const {postId} = request.params;
        console.log("Post: " + postId + " has been liked.");
        await Post.update(
            {_id: {$eq: postId}},
            {$inc: {likes: 1}}
        );
        return response.sendStatus(200);
});

portfolioServer.put("/api/posts/dislike/:postId", authenticate, async (request, response) => {
        const {postId} = request.params;
        console.log("Post: " + postId + " has been disliked.");
        await Post.update(
            {_id: {$eq: postId}},
            {$inc: {dislikes: 1}}
        );
        return response.sendStatus(200);
});

portfolioServer.get('/*', (request, response) => {

    const indexPath = path.join(clientAppDirectory, 'index.html');

    return response.sendFile(indexPath);
});

const port =  5000;
portfolioServer.listen(process.env.PORT || port, () => console.log("The server has started on localhost:" + port));