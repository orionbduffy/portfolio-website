const Mongoose = require("mongoose");

const postSchema = new Mongoose.Schema({
    username: String,
    date: {type: Date, default: Date.now},
    message: String,
    likes: {type: Number, default: 0},
    dislikes: {type: Number, default: 0}
});

const postModel = Mongoose.model("Post", postSchema);

module.exports = postModel;