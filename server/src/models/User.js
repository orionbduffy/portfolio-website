const Mongoose = require("mongoose");

const userSchema = new Mongoose.Schema({
    emailAddress: String,
    username: String,
    password: String,
    creationDate: {type: Date, default: Date.now}
});

const userModel = Mongoose.model("User", userSchema);

module.exports = userModel;