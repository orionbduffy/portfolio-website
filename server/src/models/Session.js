const Mongoose = require("mongoose");

const sessionSchema = new Mongoose.Schema({
    sessionId: String,
    userId: Mongoose.ObjectId,
    creationDate: {type: Date, expires: '7d', default: Date.now}
});

const sessionModel = Mongoose.model("Session", sessionSchema);

module.exports = sessionModel;