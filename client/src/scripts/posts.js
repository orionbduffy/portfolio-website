import React from 'react';
import '../styles/Posts.css';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import ThumbDownAltIcon from '@material-ui/icons/ThumbDownAlt';
import SendPost from "../input/SendPost";
import Axios from "axios";


class Posts extends React.Component {
    constructor(props){
        super(props);
        this.state = {posts: [], postDraft: ""};

        this.handlePostDraftChange = this.handlePostDraftChange.bind(this);
        this.handlePostButtonClick = this.handlePostButtonClick.bind(this);
        this.handleLikeClick = this.handleLikeClick.bind(this);
        this.handleDislikeClick = this.handleDislikeClick.bind(this);
        this.postForm = this.postForm.bind(this);
        this.refreshPosts = this.refreshPosts.bind(this);
    }

    componentDidMount() {
        this.refreshPosts();
    }

    async refreshPosts() {
        try {
            const response = await Axios.get("/api/posts");
            const {data} = response;
            this.setState({posts: data});
        } catch (error) {

            console.error(error.message);
        }
    }

    async handleLikeClick (postId)
    {
        try {
            await Axios.put("/api/posts/like/" + postId);
        } catch (error) {

            console.error(error.message);
        }
        this.refreshPosts();
    }

    async handleDislikeClick (postId)
    {
        try {
            await Axios.put("/api/posts/dislike/" + postId);
        } catch (error) {

            console.error(error.message);
        }
        this.refreshPosts();
    }

    postForm (post)
    {
        const {_id : postId} = post;
        const likeIcon = this.state ? <ThumbUpAltIcon fontSize={"large"}></ThumbUpAltIcon> : <ThumbUpAltIcon fontSize={"medium"}></ThumbUpAltIcon>
        const dislikeIcon = this.state ? <ThumbDownAltIcon fontSize={"large"}></ThumbDownAltIcon> : <ThumbDownAltIcon fontSize={"medium"}></ThumbDownAltIcon>

        return (
            <Grid item xs={12}>
                <Paper className={"postPaper"}>
                    <Grid item>
                        <div className={"postDiv"}>
                            <div className={"postInfo"}>
                                <h5>
                                    <span className={"nameStyle"}>{post.username.valueOf()}</span>
                                    <span className={"dateStyle"}>{post.date}</span>
                                </h5>
                            </div>
                            <div>
                                <p>{post.message.valueOf()}</p>
                            </div>
                            <div>
                                <button value = {post._id} onClick = {() => (this.handleLikeClick(postId))} >
                                    {likeIcon}{post.likes}
                                </button>
                                <button value = {post._id} onClick = {() => (this.handleDislikeClick(postId))} >
                                    {dislikeIcon}{post.dislikes}
                                </button>
                            </div>
                        </div>
                    </Grid>
                </Paper>
            </Grid>
        );
    };

    handlePostDraftChange (event)
    {
        this.setState({postDraft: event.target.value});
    }

    async handlePostButtonClick (event)
    {
        const {username} = this.props;
        const {postDraft: message} = this.state;
        const data = {username, message};
        try {
            await Axios.post("/api/posts", data);
        } catch (error) {

        console.error(error.message);
        }
        this.refreshPosts();
    }

        render () {
                const combinedPosts = this.state.posts.map(this.postForm);
                return (
                    <div className={"mainDiv"}>
                    <h2>Posts</h2>
                    <div>{combinedPosts}</div>
                    <SendPost postDraft = {this.state.postDraft} onPostDraftChange = {this.handlePostDraftChange} onPostButtonClick = {this.handlePostButtonClick} />
                    </div>
                );
        };
}
export default Posts;