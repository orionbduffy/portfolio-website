import React from 'react';

import Axios from 'axios';

import '../styles/App.css';
import LoginPage from '../pages/LoginPage.js';
import RegisterPage from '../pages/RegisterPage.js';
import HomePage from '../pages/HomePage.js';
import ResponsePage from '../pages/ResponsePage.js';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import NavigationBar from "./NavigationBar";
import { ThemeProvider } from '@material-ui/styles';
import "@material-ui/core/colors/blue";
import ProfilePage from "../pages/ProfilePage";
import Redirect from "react-router-dom/es/Redirect";

const theme = createMuiTheme({
  palette: {
    primary: {main: '#2196f3'},
    secondary: {main: '#42a5f5'},
  },
});

class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      username : "",
      user: {},
      emailAddress: "",
      password: "",
      errorVisibility: "hidden"
    };
    this.handleEmailAddressChange = this.handleEmailAddressChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.onLoginClick = this.onLoginClick.bind(this);
    this.onLogoutClick = this.onLogoutClick.bind(this);
    this.isLoggedIn = this.isLoggedIn.bind(this);
    this.fetchUser = this.fetchUser.bind(this);
  }

  async componentDidMount(){
    await this.fetchUser();
  }

  async fetchUser(){
    const response = await Axios.get("/api/user/", {withCredentials: true});
    const { data } = response;
    this.setState({
      user: data,
      emailAddress: "",
      password: "",
    });
  }

  isLoggedIn(){
    const {user} = this.state;
    return user;
  }

  handleEmailAddressChange(event) {
    const newEmailAddress = event.target.value;
    this.setState({ emailAddress: newEmailAddress });
  }

  handlePasswordChange(event) {
    const newPassword = event.target.value;
    this.setState({ password: newPassword });
  }


  async onLoginClick(){
    const {emailAddress, password} = this.state;
    const credentials = {emailAddress, password};
    try {

      await Axios.post("/api/sessions", credentials);
      await this.fetchUser();

    } catch (error) {
      this.setState({ errorVisibility: "visible"});
    }
  }

  onLogoutClick(){
    Axios.delete("/api/logout", {withCredentials:true});
    this.setState({
      user: {}
    });
  }

  render(){
    const {user, emailAddress, password, errorVisibility} = this.state;
    return (
        <ThemeProvider theme={theme}>
          <BrowserRouter>
            <NavigationBar user = {user} onLogout = {this.onLogoutClick}/>
            <Switch>
              <Route path = "/login">
                <LoginPage
                    emailAddress = {emailAddress}
                    password = {password}
                    onEmailAddressChange = {this.handleEmailAddressChange}
                    onPasswordChange = {this.handlePasswordChange}
                    onLoginClick = {this.onLoginClick}
                    isLoggedIn = {this.isLoggedIn}
                    errorVisibility = {errorVisibility}
                />
              </Route>
              <Route path = "/register">
                <RegisterPage />
              </Route>
              <Route path = "/home">
                <HomePage/>
              </Route>
              <Route path = "/response">
                <ResponsePage user = {user} isLoggedIn = {this.isLoggedIn}/>
              </Route>
              <Route path = "/profile">
                <ProfilePage user = {user} isLoggedIn = {this.isLoggedIn} />
              </Route>
              <Redirect exact from = "/" to = "/home" />
            </Switch>
          </BrowserRouter>
        </ThemeProvider>
    );
  }

}

export default App;