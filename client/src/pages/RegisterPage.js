import React from 'react';
import EmailAddressInput from '../input/EmailAddressInput.js';
import PasswordInput from '../input/PasswordInput.js';
import UsernameInput from '../input/UsernameInput.js';
import Button from '@material-ui/core/Button';
import {Redirect} from 'react-router-dom';
import {withRouter} from 'react-router-dom';
import '../styles/LoginPage.css';
import Axios from "axios";

class RegisterPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username : "",
            emailAddress : "",
            password : "",
            redirectToLogin: false
        };
        this.handleEmailAddressChange = this.handleEmailAddressChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.onRegisterClick = this.onRegisterClick.bind(this);
    }

    handleEmailAddressChange(event) {
        const newEmailAddress = event.target.value;
        this.setState({ emailAddress: newEmailAddress });
    }

    handlePasswordChange(event) {
        const newPassword = event.target.value;
        this.setState({ password: newPassword });
    }

    handleUsernameChange(event) {
        const newUsername = event.target.value;
        this.setState({username: newUsername});
    }

    async onRegisterClick(){
        const {emailAddress, password, username} = this.state;
        const data = {emailAddress, username, password};
        try {

            await Axios.post("/api/users", data);
            this.setState({
                redirectToLogin: true,
                emailAddress: "",
                username: "",
                password: ""
            });

        } catch (error) {

            console.error(error.message);
        }
    }

    render(){
        const { emailAddress, username, password, redirectToLogin } = this.state;

        if (redirectToLogin){
            return <Redirect to = "/" />;
        }

        return(
            <div className = "LoginInputContainer">
                <div className = "Inputs">
                    <EmailAddressInput emailAddress = {emailAddress} onEmailChange = {this.handleEmailAddressChange}/>
                </div>
                <div className = "Inputs">
                    <UsernameInput username = {username} onUsernameChange = {this.handleUsernameChange} />
                </div>
                <div className = "Inputs">
                    <PasswordInput password = {password} onPasswordChange = {this.handlePasswordChange} />
                </div>
                <div className = "Inputs">
                    <Button variant = "contained" onClick = {this.onRegisterClick}> Register </Button>
                </div>
            </div>
        );
    }
}
export default withRouter(RegisterPage);