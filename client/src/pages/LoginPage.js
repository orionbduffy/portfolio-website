import React from 'react';
import EmailAddressInput from '../input/EmailAddressInput.js';
import PasswordInput from '../input/PasswordInput.js';
import Button from '@material-ui/core/Button';
import {withRouter} from 'react-router';
import {Link, Redirect} from 'react-router-dom';
import '../styles/LoginPage.css';

class LoginPage extends React.Component {

    render(){
        const {emailAddress, password, onEmailAddressChange, onPasswordChange, onLoginClick,
            isLoggedIn, errorVisibility} = this.props;

        if(!!isLoggedIn().username){
            return <Redirect to = "/home" />
        }

        const errorStyle = {
            color: "red",
            visibility: errorVisibility
        };

        return(
            <div className = "LoginInputContainer">
                <p style = {errorStyle}>The email address or password was incorrect.</p>
                <div className = "Inputs">
                    <EmailAddressInput emailAddress = {emailAddress} onEmailChange = {onEmailAddressChange}/>
                </div>
                <div className = "Inputs">
                    <PasswordInput password = {password} onPasswordChange = {onPasswordChange} />
                </div>
                <div className = "Inputs">
                    <Button variant = "contained" onClick = {onLoginClick}> Login </Button>
                </div>
                <p> Not a user? <Link to = "/register"> Sign up! </Link> </p>
            </div>
        );
    }
}
export default withRouter(LoginPage);