import React from 'react';
import {withRouter} from 'react-router-dom';
import '../styles/HomePage.css';

class HomePage extends React.Component{

    render(){

        return(
            <div className={"mainDiv"}>
                <h2 style={{ textAlign: 'center' }}>Home Page</h2>
                <div className={"secondaryDiv"}>
                    <div className={"tertiaryDiv"}>
                        <h3 style={{ textAlign: 'center' }}>Overview</h3>
                        <p>
                            Hello, and welcome to my portfolio website! I know it's not much, but I'm not exactly a senior developer.
                            As of the time I'm writing this, I'm actually only sophomore in my college.
                            However, I do currently have over a 3.6 GPA, and if only my programming classes were counted it would be far higher.
                            Unfortunately, I don't know what else to write for this project, so I'll just put a picture I like up.
                        </p>
                        <img src = {require("../imgs/PrincessBrideBestMovie.jpg")} alt = "The Princess Bride is an amazing movie"/>
                    </div>
                    <div className={"tertiaryDiv"}>
                        <h3 style={{ textAlign: 'center' }}>Works</h3>
                        <h4>Bitbucket Page: </h4>
                        <p>
                            Whoops, my bitbucket page has my name on it, and I'm not supposed to include that for this project.
                            Not to mention, all of my current repositories are private.
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(HomePage);