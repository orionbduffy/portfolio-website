import React from 'react';
import Posts from "../scripts/posts";
import {withRouter, Redirect} from 'react-router-dom';

class ResponsePage extends React.Component{

    render(){
        const {user} = this.props;
        const {username} = user;
        if (!!username === false){ //keeps access restricted to logged in users.
            return <Redirect to = "/" />;
        }
        return(
            <Posts username={username}/>
        );
    }
}

export default withRouter(ResponsePage);